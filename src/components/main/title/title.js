import "./style.scss";
import { useContext } from "react";
import { BookContext } from "../../../context";

const Title = () => {
  const { data } = useContext(BookContext);

  console.log(data.active,);

  return <h2 className="title">{(data.bookData?.genreList || []).filter(e => e.id === data.active)?.[0]?.genreName}</h2>
}

export default Title;