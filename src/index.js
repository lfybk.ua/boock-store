import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.scss';
import App from './components/app';
import { BrowserRouter } from "react-router-dom";

import { WidthContextProvider, BookContextProvider, BasketContextProvider } from "./context";

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <BrowserRouter>
    <BasketContextProvider>
      <WidthContextProvider>
        <BookContextProvider>
          <App />
        </BookContextProvider>
      </WidthContextProvider>
    </BasketContextProvider>
  </BrowserRouter>

);


