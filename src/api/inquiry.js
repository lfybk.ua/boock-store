import message from "./message"

const inquiry = {
  async get({ url, headers = {}, query = "", param = "" }) {
    try {
      const response = await fetch(`${url}${param}${query}`, {
        headers: {
          method: 'GET',
          ...headers
        }
      })

      if (response.status === 500 || response.status === 404) {
        throw new Error(response.message);
      }

      const data = response.json()

      return data;
    } catch (err) {
      console.log(err);

      alert(message.httpERROR)
    }
  },
  async post({ url, sendData, headers = {}, query = "", param = "" }) {
    try {
      const response = await fetch(`${url}${param}${query}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          ...headers
        },
        body: JSON.stringify(sendData)
      })

      if (response.status === 500) {
        throw new Error(response.message);
      }

      const data = response.json()

      return data;
    } catch (err) {
      console.log(err);

      alert(message.httpERROR)
    }
  }
}

export default inquiry;