import "./style.scss";

const Footer = () => {
  return (
    <footer className="footer">
      <p className="footer__text">Безкоштовно по Україні 0784175094</p>
      <p className="footer__contact">©2022 Усі права захищено</p>
    </footer>
  )
}

export default Footer;