import { useContext } from "react";

import ListItem from "../list-item";

import { BasketContext } from "../../../context";

import "./style.scss";

const List = () => {
  const { basket } = useContext(BasketContext)

  const basketArray = Object.entries(basket)

  return (
    <div className="list-basket">
      <h2 className="list-basket__title">Ваше замовлення</h2>
      {
        basketArray[0] ? <>
          {basketArray.map((e, i) => {
            return (<ListItem key={i + "bas"} id={e[0]} data={e[1]} />)
          })}
          <div className="list-basket__box">
            <p className="list-basket__sum">Всього до сплати: {basketArray.reduce((ac, [, e]) => ac + e.counter * e.cost, 0)}грн</p>
          </div>
        </> : <p className="list-basket__text">Кошик порожній. Перейдіть на сторінку товару та оберіть книжки, які вас зацікавили.</p>
      }
    </div>
  )
}

export default List;