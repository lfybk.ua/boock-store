import { useContext, useEffect, useRef } from "react";

import "./style.scss";

import { BookContext } from "../../../context";


const Pagination = () => {
  const { view, data, changePage } = useContext(BookContext);

  const pageOfAll = Math.ceil(view.length / 4)

  let page = []

  for (let i = 0; i < pageOfAll; i++) {
    page.push({ id: i, text: i + 1 })
  }

  return (
    <div className="pagination">
      {page.map((e, i) => {
        return <p
          onClick={(e) => changePage(+e.target.dataset.id)}
          key={i + "pg"}
          data-id={e.id}
          className={"pagination__item " + (data.page === e.id ? "pagination__item-active" : "")}
        >{e.text}</p>
      })}
    </div>
  )
}

export default Pagination;