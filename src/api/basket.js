import url from "./url"
import inquiry from "./inquiry";

const basket = {
  async set(data) {
    return await inquiry.post({
      url: url.basket,
      sendData: data
    }) || null
  }
}

export default basket;