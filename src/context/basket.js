import { createContext, useEffect, useState } from "react"

export const BasketContext = createContext({});


export function BasketContextProvider({ children }) {
  const [basket, setBasket] = useState(
    JSON.parse(localStorage.getItem("basket")) || {}
  )

  useEffect(() => {
    localStorage.setItem("basket", JSON.stringify(basket))
  }, [basket])

  const setItem = ({ id, cost, isbn, name }) => {
    const temp = { ...basket };
    temp[id] = { cost: cost, counter: 1, isbn: isbn, name }
    setBasket(temp)
  }

  const deleteItem = (id) => {
    const temp = { ...basket };
    delete temp[id];
    setBasket(temp)
  }

  const plusCount = (id) => {
    const temp = { ...basket };
    if (temp[id].counter > 3) return;

    temp[id].counter++;

    setBasket(temp)
  }

  const minusCount = (id) => {
    const temp = { ...basket };
    if (temp[id].counter === 1) return;

    temp[id].counter--;

    setBasket(temp)
  }

  const clear = () => {
    setBasket({})
  }

  return (
    <BasketContext.Provider
      value={{
        basket,
        setItem,
        deleteItem,
        plusCount,
        minusCount,
        clear
      }}>
      {children}
    </BasketContext.Provider>
  )
}