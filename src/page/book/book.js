import { useContext, useEffect } from "react";
import { useParams } from "react-router-dom";

import { Poster, Description } from "../../components/book";
import { BookContext } from "../../context";

import "./style.scss";

const Book = () => {
  const params = useParams();
  const { data, getBook } = useContext(BookContext)

  useEffect(() => {
    getBook(+params.id);
  }, [data.bookData])

  return (
    <section className="book">
      <Poster />
      <Description />
    </section>
  )
}

export default Book;