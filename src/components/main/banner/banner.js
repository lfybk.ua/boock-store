import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import "./style.scss";

const sliderItem = [
  {
    src: "images/1.jpg",
    text: "Тендітна рівновага - Вдень вона рятує людям життя...",
    to: "/link"
  },
  {
    src: "images/2.jpg",
    text: "Жорстокий принц - Граційні й величні фейрі",
    to: "/link"
  },
  {
    src: "images/3.jpg",
    text: "Сага про відьмака",
    to: "/link"
  },
  {
    src: "images/4.jpg",
    text: "НЕ НИЙ + НЕ ТУПИ + НІ СИ + НІ ЗЯ + ЛЮБИ",
    to: "/link"
  },
]

const Banner = () => {
  const [slide, setSlide] = useState(0);

  useEffect(() => {
    setTimeout(() => {
      if (slide < sliderItem.length - 1) {
        setSlide(slide + 1)
      } else {
        setSlide(0)
      }
    }, 10000)
  }, [slide])

  return (
    <section className="banner">
      <div className="banner__slider">
        {sliderItem.map((e, i) => {
          return <div
            className="banner__slider-item"
            style={{
              "backgroundImage": `url('${e.src}')`,
              "right": `${slide * 100}%`,
            }}
          >
            <div className="banner__text">
              <Link to={e.to}>{e.text}</Link>
            </div>
          </div>
        })}
      </div>

    </section>
  )
}

export default Banner;


