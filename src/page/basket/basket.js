import { List, Form } from "../../components/basket";

import "./style.scss";

const Basket = () => {
  return (
    <section className="basket">
      <List />
      <Form />
    </section>
  )
}

export default Basket;