import { useContext } from "react";

import { BookContext, BasketContext } from "../../../context";

import "./style.scss";

const Poster = () => {
  const { data } = useContext(BookContext)
  const { basket, setItem, deleteItem } = useContext(BasketContext)

  const style = {
    backgroundImage: `url('${data?.currentBook?.img}')`
  }


  return (
    <div className="poster">
      <div className="poster__img" style={style}></div>
      <button
        onClick={() => {
          if (basket[data.currentBook.id]) {
            deleteItem(data.currentBook.id)
          } else {
            setItem(data.currentBook)
          }
        }}
        className="poster__btn"
      >
        {basket[data?.currentBook?.id] ? "Видалити з кошика" : "Додати в кошик"}
      </button>

    </div>
  )
}

export default Poster;