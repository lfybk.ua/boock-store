import { Routes, Route } from "react-router-dom";

import Header from "../header";
import Footer from "../footer";

import Main from "../../page/main"
import Book from "../../page/book"
import Basket from "../../page/basket"
import Delivery from "../../page/delivery"
import AboutUs from "../../page/about-us"

import "./style.scss";

const App = () => {
  return (
    <>
      <Header />
      <main className="main">
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/book/:id" element={<Book />} />
          <Route path="/basket" element={<Basket />} />
          <Route path="/delivery" element={<Delivery />} />
          <Route path="/about" element={<AboutUs />} />
        </Routes>
      </main>
      <Footer />
    </>
  )
}

export default App;