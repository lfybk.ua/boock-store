const mainURL = "http://localhost:5050/api"

const url = {
  book: `${mainURL}/book`,
  basket: `${mainURL}/basket`,
}

export default url;