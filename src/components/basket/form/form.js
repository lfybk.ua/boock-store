import { useRef } from "react";

import { useContext } from "react";
import { BasketContext } from "../../../context";

import basketAPI from "../../../api/basket"

import "./style.scss";

const Form = () => {
  const { basket, clear } = useContext(BasketContext)

  const send = async (e) => {
    e.preventDefault()

    const errorList = []

    if (!ferstName.current.value || !/^[a-zA-ZZа-юА-Ю]{1,20}$/.test(ferstName.current.value)) {
      errorList.push(ferstName.current)
    }

    if (!lastName.current.value || !/^[a-zA-ZZа-юА-Ю]{1,20}$/.test(lastName.current.value)) {
      errorList.push(lastName.current)
    }

    if (!phone.current.value || !/^\+380[0-9]{9}$|^380[0-9]{9}$|^[0-9]{10}$/.test(phone.current.value)) {
      errorList.push(phone.current)
    }

    if (!email.current.value || !/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email.current.value)) {
      errorList.push(email.current)
    }

    if (!city.current.value || !/^[a-zA-Zа-юА-Ю]{1,20}$/.test(city.current.value)) {
      errorList.push(city.current)
    }

    if (!postOffice.current.value || postOffice.current.value.length < 3 || postOffice.current.value.length > 30) {
      errorList.push(postOffice.current)
    }

    [
      ferstName.current,
      lastName.current,
      phone.current,
      email.current,
      city.current,
      postOffice.current,
    ].forEach((el) => el.classList.remove("form__error"))

    if (errorList[0]) {
      for (let item of errorList) {
        item.classList.add("form__error")
      }

      return;
    }

    const data = {
      ferstName: ferstName.current.value,
      lastName: lastName.current.value,
      phone: phone.current.value,
      email: email.current.value,
      city: city.current.value,
      postOffice: postOffice.current.value,
      text: text.current.value,
      pay: vdo.current.checked ? "department" : "card",
      delivery: vd.current.checked ? "department" : "postmat",
      data: basket,
    }

    try {
      if (!Object.entries(basket)[0]) {
        alert("Ваш кошик порожній")
        return;
      }
      await basketAPI.set(data)

      alert("Перевірте свою єлектронну адресу. Для підтвердження замовлення ми вам зателефонуемо [09:00-20:00]")
      clear()
    } catch (er) {
      alert("Виникла помилка. Перезавантажте сторінку, та спробуйте знову створити замовлення. ):")
    }
  }

  const ferstName = useRef();
  const lastName = useRef();
  const phone = useRef();
  const email = useRef();
  const vd = useRef();
  const city = useRef();
  const postOffice = useRef();
  const vdo = useRef();
  const text = useRef();

  return (
    <form className="form">
      <div>
        <p className="form__title">Ваші контактні дані</p>
        <div className="form__group">
          <label htmlFor="ferstName">Ім'я</label>
          <input ref={ferstName} id="ferstName" type="text" placeholder="Ім'я" />
        </div>
        <div className="form__group">
          <label htmlFor="lastName">Прізвище</label>
          <input ref={lastName} id="lastName" type="text" placeholder="Прізвище" />
        </div>
        <div className="form__group">
          <label htmlFor="phone">Номер телефону</label>
          <input ref={phone} id="phone" type="phone" placeholder="Номер телефону" />
        </div>
        <div className="form__group">
          <label htmlFor="email">Email</label>
          <input ref={email} id="email" type="email" placeholder="email" />
        </div>
      </div>
      <div>
        <p className="form__title">Доставка</p>
        <p>Нова пошта</p>
        <div className="form__group">
          <label htmlFor="vd">Відділення Нової пошти</label>
          <input defaultChecked value="postOffice" ref={vd} id="vd" name="type" type="radio" />
        </div>
        <div className="form__group">
          <label htmlFor="ph">Почтомат Нової пошти</label>
          <input value="pastmat" id="ph" name="type" type="radio" />
        </div>
      </div>
      <div className="form__group">
        <label htmlFor="city">Місто</label>
        <input ref={city} id="city" type="text" placeholder="Місто" />
      </div>
      <div className="form__group">
        <label htmlFor="postOffice">Номер відділення/почтомата</label>
        <input ref={postOffice} id="postOffice" type="text" placeholder="Відділення/почтомат" />
      </div>
      <div>
        <p className="form__title">Оплата</p>
        <div className="form__group">
          <label htmlFor="vdo">У відділенні</label>
          <input defaultChecked value="post" ref={vdo} id="vdo" name="pay" type="radio" />
        </div>
        <div className="form__group">
          <label htmlFor="pho">На карту</label>
          <input value="card" id="pho" name="pay" type="radio" />
        </div>
      </div>
      <div className="form__group form__text">
        <label htmlFor="text">Коментар (за бажанням)</label>
        <textarea ref={text} id="text" placeholder="Коментарь"></textarea>
      </div>
      <input onClick={send} className="form__btn" type="submit" value="Оформити замовлення" />
    </form>
  )
}


export default Form;