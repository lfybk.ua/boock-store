import "./style.scss";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header className="header">
      <div className="header__container">
        <h1 className="header__title"><Link to="/">Book World</Link></h1>
        <div className="header__box">
          <Link className="header__link" to="/delivery">
            Деталі доставки
          </Link>
          <Link className="header__link" to="/about">
            Про нас
          </Link>
          <Link to="/basket">
            <i className="header__basket fa-solid fa-basket-shopping"></i>
          </Link>
        </div>
      </div>
    </header>
  )
}

export default Header;