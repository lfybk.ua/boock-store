import "./style.scss";

import { Banner, Title, Filter, List, Pagination } from "../../components/main"

const Main = () => {
  return (
    <section className="main">
      <Banner />
      <div className="main__box">
        <Title />
        <Filter />
        <List />
        <Pagination />
      </div>

    </section>
  )
}

export default Main;