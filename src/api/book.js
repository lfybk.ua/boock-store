import url from "./url"
import inquiry from "./inquiry";

const book = {
  async getBook() {
    return await inquiry.get({
      url: url.book,
    }) || null
  }
}

export default book;