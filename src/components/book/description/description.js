import { useContext } from "react";

import { BookContext } from "../../../context";

import "./style.scss";

const Description = () => {
  const { data } = useContext(BookContext)

  const book = data.currentBook || {};

  return (
    <div className="description">
      <h2 className="description__title">{book.name}</h2>
      <p className="description__cost">{book.cost}грн</p>
      <p className="description__parm"><span className="description__type">Автор:</span> {book.author}</p>
      <p className="description__parm"><span className="description__type">Формат:</span> {book.Format}</p>
      <p className="description__parm"><span className="description__type">Ілюстрації:</span> {book.illustrations}</p>
      <p className="description__parm"><span className="description__type">isbn:</span> {book.isbn}</p>
      <p className="description__parm"><span className="description__type">Тип:</span> {book.type}</p>
      <p className="description__parm"><span className="description__type">Рік публікації:</span> {book?.yearPublication?.slice?.(0, 4)}</p>
      <p className="description__parm"><span className="description__type">Виданеицтво:</span> {book.publishingHouse}</p>
      <p className="description__parm"><span className="description__type">Мова:</span> {book.language}</p>
      <p className="description__parm"><span className="description__type">Палітурка:</span> {book.Binding}</p>
      <p className="description__parm"><span className="description__type">Кількість сторінок:</span> {book.numberPages} ст</p>
      <p className="description__text">{book.description}</p>
    </div>
  )
}

export default Description;