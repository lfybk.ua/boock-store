import { useContext, useEffect, useRef } from "react";

import "./style.scss";

import { BookContext, WidthContext } from "../../../context";

const Mobail = ({ data, active, change }) => {
  const refSelect = useRef(null)

  useEffect(() => {
    refSelect.current.value = active
  }, [active])

  return (
    <select onChange={(e) => change(+e.target.value)} ref={refSelect} className="filter__categorie-select">
      <option value="-1">Оберіть категорію</option>
      {data.map((e, i) => {
        return (
          <option key={i + "mbfl"} value={e.id}>{e.genreName}</option>
        )
      })}
    </select>
  )
}

const Desctop = ({ data, active, change }) => {
  return (
    <ul className="filter__categorie-list">
      {data.map((e, i) => {
        return (
          <li key={i + "dsfl"}>
            <button
              onClick={(e) => {
                e.preventDefault()
                change(+e.target.value)
              }}
              value={e.id}
              className={"filter__categorie-btn" + (active === e.id ? " filter__btn-active" : "")}
            >{e.genreName}</button>
          </li>
        )
      })}
    </ul>
  )
}

const Filter = () => {
  const { isXM } = useContext(WidthContext);
  const { data, changeFilter, searchFilter } = useContext(BookContext);

  const refSrearch = useRef(null);
  useEffect(() => {
    refSrearch.current.value = "";
  }, [data.active])

  return (
    <form className="filter">
      <div className="filter__box">
        <label className="filter__search-titel" htmlFor="search" >Пошук</label>
        <input
          onChange={(e) => searchFilter(e.target.value)}
          onFocus={(e) => searchFilter(e.target.value)}
          ref={refSrearch}
          className="filter__search"
          id="search"
          type="text"
          placeholder="Search..."
        />
      </div>
      <div className="filter__box">
        <p className="filter__categorie">Категорії</p>
        {isXM() ?
          <Mobail change={changeFilter} active={data.active} data={data?.bookData?.genreList || []} /> :
          <Desctop change={changeFilter} active={data.active} data={data?.bookData?.genreList || []} />
        }
      </div>
    </form>
  )
}

export default Filter;