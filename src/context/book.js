import { createContext, useEffect, useState } from "react"

import book from "../api/book";

export const BookContext = createContext({});

export function BookContextProvider({ children }) {
  const [data, setData] = useState({
    page: 0,
    active: 0,
    search: ""
  })

  const [view, setView] = useState([])

  useEffect(() => {
    if (!data.bookData) return;

    if (data.search) {
      setView(
        data.bookData.book.filter(el => el.name.toLowerCase().indexOf(data.search) !== -1)
      )
    } else if (data.active === -1) {
      setView(data.bookData.book)
    } else {
      const bookList = data.bookData.genre.filter(e => e.idGanre === data.active).map(e => e.idBook);

      setView(
        data.bookData.book.filter(e => bookList.includes(e.id))
      )
    }
  }, [data.active, data.search])

  useEffect(() => {
    const ferchData = async () => {
      const bookData = await book.getBook();

      setData({
        ...data,
        active: bookData.genreList[0].id,
        bookData: bookData,
      })
    }

    ferchData()
  }, [])

  const changeFilter = (id) => {
    setData({
      ...data,
      active: id,
      page: 0,
      search: ""
    })
  }

  const searchFilter = (str) => {
    setData({
      ...data,
      page: 0,
      active: -1,
      search: str
    })
  }

  const changePage = (page) => {
    setData({
      ...data,
      page: page,
    })
  }

  const getBook = (id) => {
    if (!data.bookData) return;

    for (let book of data.bookData.book) {
      if (book.id === id) {
        setData({
          ...data,
          currentBook: book
        })
        return;
      }
    }
  }

  return (
    <BookContext.Provider
      value={{
        data,
        view,
        changeFilter,
        searchFilter,
        changePage,
        getBook,
      }}>
      {children}
    </BookContext.Provider>
  )
}