import "./style.scss";

const AboutUs = () => {
  return (
    <section className="about-us">
      <div className="about-us__header">
        <h2 className="about-us__title">Надійний у книгах друг!</h2>
        <p className="about-us__sub-title">Наша мета – стати для Вас найкращим другом з пошуку та купівлі книг за адекватну вартість!</p>
      </div>
      <div className="about-us__box">
        <h3 className="about-us__logo">Book World</h3>
        <div className="about-us__text-box">
          <p className="about-us__text-title">Book world</p>
          <p>Інтернет-магазин з доставкою по всій Україні!</p>
          <p>Ми любимо книги та піклуємося, щоб Ви отримали свою книгу швидко та за найвигіднішою ціною</p>
          <p>У нас в Асортименті понад 1000 різноманітних книг, серед яких Ви точно знайдете те, що вам потрібне!</p>
          <p>Зробіть замовлення на сайті або напишіть нам у Instagram. Будемо раді допомогти із вибором книг!</p>
          <p>Все буде Україна!</p>
        </div>
      </div>
    </section>
  )
}

export default AboutUs;