import { useContext } from "react";
import { Link } from "react-router-dom";

import ItemBook from "../item-book";

import { BookContext } from "../../../context";

import "./style.scss";

const List = () => {
  const { view, data } = useContext(BookContext);

  const startPage = data.page * 4;

  return (
    <div className="list">
      {
        view.length !== 0 ?
          view.slice(startPage, startPage + 4).map((e, id) => {
            return (
              <Link className="list__link" key={id + "ib"} to={`book/${e.id}`}>
                <ItemBook data={e} />
              </Link>
            )
          }) :
          <p>Not Found</p>
      }
    </div>
  )
}

export default List;