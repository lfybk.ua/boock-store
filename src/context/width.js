import { createContext, useRef, useState } from "react"
import { widthMobile, widthXM } from "../util";

export const WidthContext = createContext({});


export function WidthContextProvider({ children }) {
  const [width, setWidth] = useState(window.innerWidth)
  const timeout = useRef(null);

  const chengeWidth = () => {
    clearTimeout(timeout.current)

    timeout.current = setTimeout(() => {
      setWidth(window.innerWidth);
    }, 100)
  }

  window.addEventListener('resize', chengeWidth)

  const isMobile = () => {
    return widthMobile >= width //|| /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(window.navigator.userAgent)
  }

  const isXM = () => {
    return widthXM >= width //|| /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(window.navigator.userAgent)
  }

  return (
    <WidthContext.Provider
      value={{
        width,
        isMobile,
        isXM
      }}>
      {children}
    </WidthContext.Provider>
  )
}