import { Link } from "react-router-dom";

import "./style.scss";

const ItemBook = ({ data }) => {
  const style = {
    backgroundImage: `url('${data.img}')`
  }

  return (
    <div className="book-item">
      <div>
        <div className="book-item__img" style={style}></div>
      </div>
      <div className="book-item__box">
        <p className="book-item__title">{data.name}</p>
        <div className="book-item__data">
          <i className="far fa-user book-item__name"> {data.author}</i>
          <i className="far fa-credit-card">{data.cost}грн</i>
        </div>
        <p className="book-item__text"> {data.briefDescription}</p>
      </div>
    </div>
  )
}

export default ItemBook