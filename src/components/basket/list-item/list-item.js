import { useContext } from "react";

import { BasketContext } from "../../../context";

import "./style.scss";

const ListItem = ({ data, id }) => {
  const { deleteItem, plusCount, minusCount } = useContext(BasketContext);

  return (
    <div className="basket-item">
      <p className="basket-item__text">{data.name} </p>
      <div className="basket-item__container">
        <p className="basket-item__text basket-item__sum" ><span className="basket-item__cost">{data.cost * data.counter}</span>грн.</p>
        <div className="basket-item__box">
          <button disabled={data.counter > 3} onClick={() => plusCount(id)} className="basket-item__btn basket-item__btn-plus"><i className="fa-solid fa-plus"></i></button>
          <input className="basket-item__count" type="text" value={data.counter} disabled />
          <button disabled={data.counter <= 1} onClick={() => minusCount(id)} className="basket-item__btn basket-item__btn-minus"><i className="fa-solid fa-minus"></i></button>
        </div>
        <i onClick={() => deleteItem(id)} className="basket-item__icon fa-solid fa-xmark"></i>
      </div>
    </div >
  )
}

export default ListItem;